/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

module.exports = function(callback,config){
   var version = 1;
   var name = config.name;
   var dgram = require('dgram');
   var inSock = dgram.createSocket('udp4');
   var outSock = dgram.createSocket('udp4');
   var thiz = this;
   
   inSock.on("error", function (err) {
       console.log("Input discovery socket error");
       inSock.close();
       thiz.close();
   });
    
   inSock.on("message", function (msg, rinfo) {
       
       var packet = JSON.parse(msg);
       if(packet.version> version) {
           console.log("Unsoported protocol version " + packet.version 
                   + "; Current version is " + version);
           return;
       }
       if(packet.action==="discovery") {
           var b = new Buffer("!" + name);
           outSock.send(b,0,b.length,config.ports.discovery,rinfo.address);
       }
       if(packet.action==="connect") {
           callback(rinfo.address);
       }
   });
   outSock.on("error", function (err) {
       console.log("Output discovery socket error");
       inSock.close();
       thiz.close();
   });
   
  inSock.bind(config.ports.discovery);
  var opened = true;
  
  this.close = function() {
      if(opened) {
        opened = false;
        inSock.close();
        outSock.close();
      }
  };
   
};
