module.exports = function(address, config) {
    console.log("Starting video stream");
    
        
    var child_process = require('child_process');
    
    var port = config.ports.video;
    var command = config.videoCommand;
    
    command = command.replace('%port%',port).replace('%ip%',address);
    var running = true;
    var handler = null;
    var run = function() {
        if(!running) return;
        handler = child_process.exec(command,function() {
            if(running)
                setTimeout(run,500);
            else
                handler = null;
        });
    };
    run();
    
    this.close = function() {
        console.log("Stoping video stream");
        running = false;
        if(handler !== null)
            handler.kill('SIGINT');
    };
    
};