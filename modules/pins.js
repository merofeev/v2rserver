module.exports = function(connection){
    var fs = require('fs');
    var fd = fs.openSync('/dev/v2r_pins','w');
    
    this.close = function() {

    };
    
    
    this.command = function(request) {
        if(request.action == 'set_pin' ) {
            var b = new Buffer(3);
            b[0] = 1;
            b[1] = request.pin;
            b[2] = 1 + request.value * 2;
            fs.write(fd,b,0,b.length,0,function(){});
        }
        if(request.action == 'config_pwm') {
            var b = new Buffer(2);
            b[0] = 6;
            b[1] = request.pin;
            fs.write(fd,b,0,b.length,0,function(){});
        }
        if(request.action == 'set_pwm') {
            var b = new Buffer(6);
            b[0] = 7;
            b[1] = request.pwm;
            b[2] = request.duty & 0xff;
            b[3] = request.duty >> 8;
            b[4] = request.period & 0xff;
            b[5] = request.period >> 8;
            fs.write(fd,b,0,b.length,0,function(){});
        }
    };
    this.subscribe = function(id,request) {
    };
    this.unsubscribe = function(id) {      
    };
    
};



