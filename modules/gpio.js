module.exports = function(connection){
    var fs = require('fs');
    var fd = fs.openSync('/dev/v2r_gpio','w');
    
    this.close = function() {

    };

    
    this.command = function(request) {
        var b = new Buffer(3);
        b[0] = 1;
        b[1] = request.gpio;
        b[2] = 1 + request.value * 2;
        fs.write(fd,b,0,b.length,0,function(){});
    };
    this.subscribe = function(id,request) {
    };
    this.unsubscribe = function(id) {      
    };
    
};



