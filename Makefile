REMOTE=root@192.168.1.250

copy: server.js
	scp *.js $(REMOTE):
	scp *.json $(REMOTE):
	scp -r modules/*.js $(REMOTE):modules/
run: copy
	ssh $(REMOTE) node server.js
