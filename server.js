console.log("Virt2Real Server");

var config = require("./config.json");

console.log("Server name: %s\n",config.name);

var videostream = require("./videostream.js")



var Modules = require("./modules.js");
var Connection = require("./connection.js");
var Discovery = require("./discovery.js");
var connection = null;

discovery = new Discovery(function(address){
    if(connection!==null)
          connection.close();
    console.log("Connected to " + address);
    connection = new  Connection(address,Modules,config);
},config);


process.stdin.resume();
process.stdin.setEncoding('utf8');
var running = true;
var exit = function() {
    if(running) {
        running = false;
        console.log("Quiting");
        process.stdin.destroy();
        discovery.close();
        if(connection!==null)
            connection.close();
    }
};

process.stdin.on('data', function(chunk) {
    chunk = chunk.replace(/^\s+|\s+$/g, '');
  if(chunk==="quit") {
      exit();
  }
});

process.on('exit',exit);
process.on('SIGINT',exit);
process.on('SIGTERM',exit);
