module.exports = function(address, Modules, config) {
   var dgram = require('dgram');
   var VideoStream = require("./videostream.js");
   var video = new VideoStream(address,config);
   
   var modules = [];
   this.config = config;
   var reporters = [];
   
   var subscriptions = [];
   var getModule = function(name) {
       var m = modules[name];
       if(m === undefined)
           console.log("Warning: unknown module %s",name);
       return m;
   };
   
   var inSock = dgram.createSocket('udp4');
   var outSock = dgram.createSocket('udp4');
   var thiz = this;
   
   inSock.on("error", function (err) {
       console.log("Input data socket error");
       inSock.close();
       thiz.close();
   });
    
   inSock.on("message", function (msg, rinfo) {
        var packet = JSON.parse(msg);
        
        if(packet.command !== undefined) {
            for(var i in packet.command) {
                var modn = packet.command[i].name;
                var payload = packet.command[i].payload;
                var module = getModule(modn);
                if(module !== undefined) {
                    module.command(payload);
                }
            }
        }
        
        if(packet.subscribe !== undefined) {
            for(var i in packet.subscribe) {
                var modn = packet.subscribe[i].name;
                var id = packet.subscribe[i].id;
                var payload = packet.subscribe[i].payload;
                var module = getModule(modn);
                if( (module !== undefined) && (subscriptions[id]===undefined) ) {
                    subscriptions[id] = module.subscribe(id,payload);
                }
            }
        }
        
        if(packet.unsubscribe !== undefined) {
            for(var i in packet.unsubscribe) {
                var modn = packet.unsubscribe[i].name;
                var id = packet.unsubscribe[i].id;
                var module = getModule(modn);
                if( (module !== undefined) && (subscriptions[id]!==undefined) ) {
                    module.unsubscribe(id);
                    subscriptions[id] = undefined;
                }
            }
        }
        
   });
   outSock.on("error", function (err) {
       console.log("Output data socket error");
       inSock.close();
       thiz.close();
   });
   var subTimeout = null;
   var sendSubscriptions = function() {
       var data={};
       for(var id in subscriptions){
           data[id] = subscriptions[id]();
       }
       var d = new Buffer(JSON.stringify(data));
       outSock.send(d, 0, d.length, config.ports.data, address, function(err, bytes) {});
       subTimeout = setTimeout(sendSubscriptions,config.subscriptionTimeout);
   };
   
   subTimeout = setTimeout(sendSubscriptions,config.subscriptionTimeout);
   
   for(var n in Modules) {
       modules[n] = new Modules[n](this);
   }
   
   inSock.bind(config.ports.data);
   var connected = true;
   
   this.close = function(){
       if(connected) {
           connected = false;
           video.close();
           clearTimeout(subTimeout);
           outSock.close();
           inSock.close();
           
           for(var n in Modules) {
               modules[n].close();
           }
       }
   };
    
};