var fs = require("fs");

var files = fs.readdirSync("./modules");

var modules = [];

for (var i in files) {
    var f = files[i];
    if(f.substr(-3)===".js") {
        var name = f.substr(0,f.length-3);
        modules[name] = require("./modules/" + f);
    }
}

module.exports = modules;


